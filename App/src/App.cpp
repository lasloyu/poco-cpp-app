﻿//
// App.cpp
//
// Defines the entry point for the application.
//
// Copyright (c) 2021, 
// All rights reserved.
//

#include "Audro/App/App.h"

#include <iomanip>
#include <iostream>
#include <Poco/AutoPtr.h>
#include <Poco/ConsoleChannel.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/Environment.h>
#include <Poco/ErrorHandler.h>
#include <Poco/Exception.h>
#include <Poco/File.h>
#include <Poco/FileChannel.h>
#include <Poco/FileStream.h>
#include <Poco/FormattingChannel.h>
#include <Poco/PatternFormatter.h>
#include <Poco/SplitterChannel.h>
#include <Poco/Util/HelpFormatter.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/SystemConfiguration.h>
#include <sys/stat.h>

#include "Audro/App/AppConfig.h"
#include "Version.h"

using namespace std;
using namespace Poco;
using namespace Util;
using namespace Audro::App;


class App final : public ServerApplication
{
public:
    App() : m_errorHandler(*this), m_helpRequested(false), m_versionRequested(false)
    {
        Poco::ErrorHandler::set(&m_errorHandler);
    }

    //~App() = default;

    const char* name() const override { return "App"; }

protected:
    class ErrorHandler final : public Poco::ErrorHandler
    {
    public:
        explicit ErrorHandler(App& app) :
            m_app(app)
        {
        }

        void exception(const Exception& exc) override
        {
            //// Don't log Poco::Net::ConnectionResetException and Poco::TimeoutException -
            //// getting too many of them from the web server.
            //if (std::strcmp(exc.name(), "Connection reset by peer") != 0 &&
            //    std::strcmp(exc.name(), "Timeout") != 0)
            //{
            log(exc.displayText());
            //}
        }

        void exception(const std::exception& exc) override
        {
            log(exc.what());
        }

        void exception() override
        {
            log("unknown exception");
        }

        void log(const std::string& message) const
        {
            m_app.logger().notice("A thread was terminated by an unhandled exception: " + message);
        }

    private:
        App& m_app;
    };

    void initialize(Application& self) override
    {
        if (!m_helpRequested && !m_versionRequested)
        {
            logger().information(
                "\n"
                "\n"
                "    App ver.%s\n"
                "\n"
                "    Copyright (c) 2021 \n"
                "    All rights reserved.\n", std::string(VERSION)
            );

            logger().information(
                "System information: %s (%s) on %s, %u CPU core(s).\n",
                Poco::Environment::osDisplayName(),
                Poco::Environment::osVersion(),
                Poco::Environment::osArchitecture(),
                Poco::Environment::processorCount());
        }

        initLogger();

        // Load application configuration file
        Path appConfigPath("../config/app.properties");

        if (findFile(appConfigPath)) // NOLINT(bugprone-branch-clone)
        {
            poco_trace_f(logger(), "Loading configuration from: %s", appConfigPath.absolute().toString());
            loadConfiguration(appConfigPath.toString());
        }
        else
        {
            poco_warning_f(logger(), "Missing configuration file:  %s", appConfigPath.absolute().toString());
        }        


        AppConfig::initialize(self);
        ServerApplication::initialize(self);
    }

    void uninitialize() override
    {
        if (!m_helpRequested && !m_versionRequested)
            logger().information("Shutting down ...");
        ServerApplication::uninitialize();
    }

    void defineOptions(OptionSet& options) override
    {
        ServerApplication::defineOptions(options);

        options.addOption(
            Option("help", "h", "Display help information on command line arguments.")
            .required(false)
            .repeatable(false)
            .callback(OptionCallback<App>(this, &App::handleHelp)));

        options.addOption(
            Option("version", "v", "Display version information.")
            .required(false)
            .repeatable(false)
            .callback(OptionCallback<App>(this, &App::handleVersion)));
    }

    void handleHelp(const std::string& name, const std::string& value)
    {
        m_helpRequested = true;
        displayHelp();
        stopOptionsProcessing();
    }

    void handleVersion(const std::string& name, const std::string& value)
    {
        m_versionRequested = true;
        displayVersion();
        stopOptionsProcessing();
    }

    void displayHelp() const
    {
        HelpFormatter helpFormatter(options());
        helpFormatter.setCommand(commandName());
        helpFormatter.setUsage("OPTIONS");
        helpFormatter.setHeader(
            "\n"
            "The App.\n"
            "Copyright (c) 2021 \n"
            "All rights reserved.\n\n"
            "The following command line options are supported:"
        );
        helpFormatter.setFooter(" ");
        helpFormatter.setIndent(8);
        helpFormatter.format(std::cout);
    }

    void displayVersion() const
    {
        logger().information(
            "\n"
            "    The App  ver. %s\n"
            "    Copyright (c) 2021 \n", std::string(VERSION)
        );
    }

    int main(const std::vector<std::string>& args) override
    {
        if (!m_helpRequested && !m_versionRequested)
        {
            logger().information("application.path: the absolute path to application executable");
            logger().information("application.path - %s", config().getString("application.path", std::string()));
            logger().information(std::string());

            logger().information("application.name: the file name of the application executable");
            logger().information("application.name - %s", config().getString("application.name", std::string()));
            logger().information(std::string());

            logger().information("application.baseName: the file name (excluding extension) of the application executable");
            logger().information("application.baseName - %s", config().getString("application.baseName", std::string()));
            logger().information(std::string());

            logger().information("application.dir: the path to the directory where the application executable resides");
            logger().information("application.dir - %s", config().getString("application.dir", std::string()));
            logger().information(std::string());

            logger().information("application.configDir: the path to the directory where user specific configuration files of the application should be stored.");
            logger().information("application.configDir - %s", config().getString("application.configDir", std::string()));
            logger().information(std::string());

            logger().information("application.cacheDir: the path to the directory where user specific non-essential data files of the application should be stored.");
            logger().information("application.cacheDir - %s", config().getString("application.cacheDir", std::string()));
            logger().information(std::string());

            logger().information("application.dataDir: the path to the directory where user specific data files of the application should be stored.");
            logger().information("application.dataDir - %s", config().getString("application.dataDir", std::string()));
            logger().information(std::string());

            logger().information("application.tempDir: the path to the directory where user specific temporary files and other file objects of the application should be stored.");
            logger().information("application.tempDir - %s", config().getString("application.tempDir", std::string()));

            logger().information("\n\n===============================================================================\n");

            logger().information("system.osName: the operating system name");
            logger().information("system.osName - %s", config().getString("system.osName", std::string()));
            logger().information(std::string());

            logger().information("system.osVersion: the operating system version");
            logger().information("system.osVersion - %s", config().getString("system.osVersion", std::string()));
            logger().information(std::string());

            logger().information("system.osArchitecture: the operating system architecture");
            logger().information("system.osArchitecture - %s", config().getString("system.osArchitecture", std::string()));
            logger().information(std::string());

            logger().information("system.nodeName: the node (or host) name");
            logger().information("system.nodeName - %s", config().getString("system.nodeName", std::string()));
            logger().information(std::string());

            logger().information("system.nodeId: system ID, based on the Ethernet address (format \"xxxxxxxxxxxx\") of the first Ethernet adapter found on the system.");
            logger().information("system.nodeId - %s", config().getString("system.nodeId", std::string()));
            logger().information(std::string());

            logger().information("system.currentDir: the current working directory");
            logger().information("system.currentDir - %s", config().getString("system.currentDir", std::string()));
            logger().information(std::string());

            logger().information("system.homeDir: the user's home directory");
            logger().information("system.homeDir - %s", config().getString("system.homeDir", std::string()));
            logger().information(std::string());

            logger().information("system.configHomeDir: the base directory relative to which user specific configuration files should be stored");
            logger().information("system.configHomeDir - %s", config().getString("system.configHomeDir", std::string()));
            logger().information(std::string());

            logger().information("system.cacheHomeDir: the base directory relative to which user specific non-essential data files should be stored");
            logger().information("system.cacheHomeDir - %s", config().getString("system.cacheHomeDir", std::string()));
            logger().information(std::string());

            logger().information("system.dataHomeDir: the base directory relative to which user specific data files should be stored");
            logger().information("system.dataHomeDir - %s", config().getString("system.dataHomeDir", std::string()));
            logger().information(std::string());

            logger().information("system.tempHomeDir: the base directory relative to which user-specific temporary files and other file objects should be placed");
            logger().information("system.tempHomeDir - %s", config().getString("system.tempHomeDir", std::string()));
            logger().information(std::string());

            logger().information("system.tempDir: the system's temporary directory");
            logger().information("system.tempDir - %s", config().getString("system.tempDir", std::string()));
            logger().information(std::string());

            logger().information("system.configDir: the system's configuration directory");
            logger().information("system.configDir - %s", config().getString("system.configDir", std::string()));
            logger().information(std::string());

            logger().information("system.dateTime: the current UTC date and time, formatted in ISO 8601 format.");
            logger().information("system.dateTime - %s", config().getString("system.dateTime", std::string()));
            logger().information(std::string());

            logger().information("system.pid: the current process ID.");
            logger().information("system.pid - %s", config().getString("system.pid", std::string()));
            logger().information(std::string());

            logger().information("system.env.<NAME>: the environment variable with the given <NAME>.");
            logger().information("system.env.USER - %s", config().getString("system.env.USER", std::string()));
            logger().information("system.env.SHELL - %s", config().getString("system.env.SHELL", std::string()));
            logger().information("system.env.PWD - %s", config().getString("system.env.PWD", std::string()));
            logger().information("system.env.HOSTNAME - %s", config().getString("system.env.HOSTNAME", std::string()));
            logger().information("system.env.HOME  - %s", config().getString("system.env.HOME", std::string()));
            logger().information("system.env.MAIL - %s", config().getString("system.env.MAIL", std::string()));
            logger().information("system.env.LANG - %s", config().getString("system.env.LANG", std::string()));
            logger().information("system.env.TZ - %s", config().getString("system.env.TZ", std::string()));
            logger().information("system.env.PS1 - %s", config().getString("system.env.PS1", std::string()));
            logger().information("system.env.TERM - %s", config().getString("system.env.TERM", std::string()));
            logger().information("system.env.DISPLAY - %s", config().getString("system.env.DISPLAY", std::string()));
            logger().information("system.env.HISTFILESIZE - %s", config().getString("system.env.HISTFILESIZE", std::string()));
            logger().information("system.env.EDITOR - %s", config().getString("system.env.EDITOR", std::string()));
            logger().information("system.env.MANPATH - %s", config().getString("system.env.MANPATH", std::string()));
            logger().information("system.env.OSTYPE - %s", config().getString("system.env.OSTYPE", std::string()));
            logger().information("system.env.NAME - %s", config().getString("system.env.NAME", std::string()));



            // wait for CTRL-C or kill
            waitForTerminationRequest();
        }

        return Application::EXIT_OK;
    }

private:
    // ReSharper disable once CppMemberFunctionMayBeConst
    void initLogger()
    {
        config().setString("application.logger", "App");

        File logsDir("../logs/");
        logsDir.createDirectory();

        AutoPtr<FileChannel> file(new FileChannel(format("%s/%s.log", logsDir.path(), string(name()))));
        file->setProperty("rotation", "10 M");
        file->setProperty("rotateOnOpen", "true");
        file->setProperty("archive", "timestamp");
        file->setProperty("times", "local");
        file->setProperty("compress", "true");

        AutoPtr<SplitterChannel> splitter(new SplitterChannel());
        splitter->addChannel(new ColorConsoleChannel());
        splitter->addChannel(file);

        const std::string format = "%L%Y-%b-%d %H:%M:%S.%F [%p] %s<%I>: %t";

        Logger::root().setLevel(Message::PRIO_TRACE);
        Logger::root().setChannel(new FormattingChannel(new PatternFormatter(format), splitter));
    }


    ErrorHandler m_errorHandler;
    bool m_helpRequested;
    bool m_versionRequested;

    friend class AppConfig;
};


int main(const int argc, char** argv)
{
    try
    {
        App app;
        return app.run(argc, argv);
    }
    catch (Exception& exc)
    {
        std::cerr << exc.displayText() << std::endl;
        return Util::Application::EXIT_SOFTWARE;
    }
}

#include "Audro/App/AppConfig.h"

using namespace std;
using namespace Audro::App;
using namespace Poco::Util;

namespace Audro
{
namespace App
{
    AppConfig::AppConfig() :
        _pConfig(new LayeredConfiguration)
    {
    }

    string AppConfig::applicationPath()
    {
        return instance().config().getString("application.path", "");
    }

    string AppConfig::applicationName()
    {
        return instance().config().getString("application.name", "");
    }

    string AppConfig::applicationDir()
    {
        return instance().config().getString("application.dir", "");
    }

    // from PlatformManagerConfig.json
    string AppConfig::platformId()
    {
        return instance().config().getString("Platform.id", "");
    }

    string AppConfig::pornName()
    {
        return instance().config().getString("portName", "/dev/ttyUSB0");
    }

    bool AppConfig::saveTelemetryToFile()
    {
        return instance().config().getBool("saveTelemetryToFile", false);
    }


    namespace
    {
        Poco::SingletonHolder<AppConfig> sh;
    }


    void AppConfig::initialize(Application& app)
    {
        instance().config().add(app.configPtr()->createView(""));
    }

    // ReSharper disable once CppInconsistentNaming
    AppConfig& AppConfig::instance()
    {
        return *sh.get();
    }
}
}

//
// AppConfig.h
//
// Library: ClassLoader
// Package: ClassLoader
// Module:  ClassLoader
//
// Definition of the AppConfig class.
//
// Copyright (c) 2021, Elbit Systems C4I and Cyber Ltd.
//


#ifndef GremsyT3V3_AppConfig_INCLUDED
#define GremsyT3V3_AppConfig_INCLUDED


#include <Poco/Logger.h>
#include <Poco/Util/Application.h>
#include <Poco/Util/LayeredConfiguration.h>
#include <string>


namespace Audro
{
namespace App
{
    class AppConfig
    {
    public:
        AppConfig();

        static std::string applicationPath();
            /// The absolute path to application executable

        static std::string applicationName();
            /// The file name of the application executable

        static std::string applicationDir();
            /// The path to the directory where the application executable resides

        static std::string platformId();
            /// Platform ID

        static std::string pornName();
            /// Serial port name

        static bool saveTelemetryToFile();
            /// Save or not gimbal telemetry to file on each snapshot trigger


        static void initialize(Poco::Util::Application& app);


        // ReSharper disable once CppInconsistentNaming
        Poco::Util::LayeredConfiguration& config() const;
            /// Returns configuration reference.

        // ReSharper disable once CppInconsistentNaming
        Poco::Util::LayeredConfiguration::Ptr configPtr() const;
            /// Returns configuration smart pointer.


    private:
        // ReSharper disable once CppInconsistentNaming
        static AppConfig& instance();


        Poco::Util::LayeredConfiguration::Ptr _pConfig;
    };


    // ReSharper disable once CppInconsistentNaming
    inline Poco::Util::LayeredConfiguration& AppConfig::config() const
    {
        return *const_cast<Poco::Util::LayeredConfiguration*>(_pConfig.get());
    }

    // ReSharper disable once CppInconsistentNaming
    inline Poco::Util::LayeredConfiguration::Ptr AppConfig::configPtr() const
    {
        return _pConfig;
    }
}
}

#endif
